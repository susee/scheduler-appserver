# README #

### What is this repository for? ###
**RESTful Web Service Using Spring Boot** - The goal is to create/practise a basic restful web service using Spring Boot that allows users to organise meetings by selecting other attendees and meeting rooms.

### Summary of the project functionality built so far ###
* Given no user with same id exists, it should store a new user in the database and immediately return the stored object.
* Given there exists a user with same id, it should not store, but return error status with the message.
* Given there are previously stored users, it should be able to retrieve the list of them.

### How to set up? ###
import the source code into Eclipse or IntelliJ or anyother IDE as maven project
mvn spring-boot:run

### How to run tests ###
curl -X POST -d '{ "id": "test_id", "password": "test_password" }' http://localhost:8080/user