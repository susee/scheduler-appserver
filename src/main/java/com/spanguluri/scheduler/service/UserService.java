package com.spanguluri.scheduler.service;

import com.spanguluri.scheduler.domain.User;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> getList();

}