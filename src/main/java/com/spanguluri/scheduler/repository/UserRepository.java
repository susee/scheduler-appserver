package com.spanguluri.scheduler.repository;

import com.spanguluri.scheduler.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
